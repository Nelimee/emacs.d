(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(reftex-ref-style-default-list '("Hyperref"))
 '(safe-local-variable-values
   '((eval setenv "LANG" "en_US.UTF-8")
     (LaTeX-command-extra-options . "-shell-escape")
     (TeX-command-extra-options . "-shell-escape"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
