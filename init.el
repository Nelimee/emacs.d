;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                   DEBUG                                    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For the moment, no debug because it is annoying on the daily-basis.
;; Can be re-set to "t" if needed.
(setq debug-on-error nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                straight.el                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; From https://github.com/raxod502/straight.el#integration-with-use-package-1
;; use-package does not need ":straight" as it is now by default.
;; Use ":straight nil" to override locally.
(setq straight-use-package-by-default t)
;; Avoid the utilisation of find(1) to check for modified packages. Use
;; a more lazy approach.
;; See https://github.com/raxod502/straight.el/blob/master/README.md#customizing-when-packages-are-built
(setq straight-check-for-modifications '(check-on-save))
;; From https://github.com/raxod502/straight.el#getting-started
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                use-package                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://ianyepan.github.io/posts/setting-up-use-package/
(straight-use-package 'use-package)
;; See https://github.com/raxod502/straight.el#integration-with-use-package-1
(setq straight-use-package-by-default t)
(require 'bind-key)                ;; if you use any :bind variant
(require 'package)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                 custom.el                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                              packages-config                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path "~/.emacs.d/packages-config/")

;; First load GC-related stuffs
(load "gcmh-config.el")

;; Emacs generic configuration
(load "generic-config.el")                    ;; Generic configuration like UI, font or backups

(load "disable-mouse-config.el")
(load "all-the-icons-config.el")              ;; Icons in emacs

(load "theme-config.el")                      ;; UI theme configuration
(load "modeline-config.el")                   ;; doom-modeline
(load "whichkey-config.el")                   ;; Pops up available keys for commands
(load "smartparens-config.el")                ;; Making parenthesis smarter
(load "flycheck-config.el")    
(load "ispell-config.el")                     ;; Checking orthograph
(load "dashboard-config.el")                  ;; Nice dashboard at the start of emacs
(load "ace-window-config.el")                 ;; Window management
(load "fill-column-indicator-config.el")      ;; Window management
(load "powerthesaurus-config.el")             ;; Findings synonyms online
;; (load "esup-config.el")                       ;; Emacs startup profiler
(load "company-config.el")                    ;; Auto-completion framework
;; (load "neotree-config.el")                    ;; Tree-like view of project files
;; (load "treemacs-config.el")                   ;; Tree-like view of project files
;; (load "ivy-config.el")                        ;; Completion framework
;; (load "yasnippet-config.el")                  ;; Automatic expansion
;; (load "highlight-indent-guide-config.el")
;; (load "flx-config.el")                        ;; Fuzzy matching engine
;; (load "hs-mode-config.el")
;; (load "ido-config.el")

;; Developement environments
(load "projectile-config.el")
(load "lsp-config.el")
(load "c++-config.el")             ;; C++ language mode
(load "python-config.el")          ;; Python language mode
(load "scala-config.el")           ;; Scala language mode
(load "cmake-config.el")           ;; CMake language mode
(load "git-config.el")
(load "org-config.el")
(load "latex-config.el")
(load "markdown-config.el")
(load "dockerfile-config.el")
(load "yaml-config.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                           General optimisations                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; See https://github.com/emacs-lsp/lsp-mode#performance
;; (setq read-process-output-max (* 5 1024 1024)) ;; 5 MB
(put 'narrow-to-region 'disabled nil)
