(use-package magit
  :defer t
  :hook
  ;; Automatically refresh magit status buffer on save.
  ;; See https://magit.vc/manual/magit/Automatic-Refreshing-of-Magit-Buffers.html#Automatic-Refreshing-of-Magit-Buffers
  (after-save . magit-after-save-refresh-status)
  :bind (("C-c g" . magit-status))
  )


(use-package evil-magit
  :defer t
  :requires magit)

(use-package git-modes
  :requires magit)
