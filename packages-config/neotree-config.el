
(defun nelimee--neotree-resize-window (&rest _args)
  "Resize neotree window.
https://github.com/jaypei/emacs-neotree/pull/110"
  (interactive)
  (neo-buffer--with-resizable-window
   (let ((fit-window-to-buffer-horizontally t))
     (fit-window-to-buffer))))


(use-package neotree
  :bind (("C-c n" . neotree-projectile-action))
  :config
  ;; Change root with Projectile
  ;; See https://www.emacswiki.org/emacs/NeoTree#toc9
  (setq projectile-switch-project-action 'neotree-projectile-action)
  ;; See https://emacs.stackexchange.com/a/34758
  (setq neo-autorefresh nil)
  ;; Automatically resize Neotree window.
  ;; See https://github.com/jaypei/emacs-neotree/pull/110#issuecomment-238994385
  :hook ((neo-change-root-hook . nelimee--neotree-resize-window)
         (neo-enter-hook . nelimee--neotree-resize-window))
  )
