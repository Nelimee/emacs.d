(use-package org
  :config
  ;; Define some templates
  (setq org-startup-truncated nil)
  (setq org-startup-folded t)
  (setq org-enforce-todo-dependencies t)
  (setcar (nthcdr 4 org-emphasis-regexp-components) 10)
  (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)
  ;; https://stackoverflow.com/a/38477233
  (setq org-image-actual-width nil)
  ;; https://tex.stackexchange.com/a/10659/166093
  (setq org-latex-to-pdf-process (list "latexmk -f -pdf %f"))
  :bind (("C-c c" . org-capture)
         ("C-c l" . org-store-link)
         ("C-c a" . org-agenda))
  ;; :hook (org-mode . visual-line-mode)
  )

(use-package ox-hugo
  :ensure t
  :pin melpa
  :after ox)

;; From https://ox-hugo.scripter.co/doc/org-capture-setup/
;; Populates only the EXPORT_FILE_NAME property in the inserted headline.
(with-eval-after-load 'org-capture
  (defun org-hugo-new-subtree-post-capture-template ()
    "Returns `org-capture' template string for new Hugo post.
See `org-capture-templates' for more information."
    (let* ((title (read-from-minibuffer "Post Title: ")) ;Prompt to enter the post title
           (fname (org-hugo-slug title)))
      (mapconcat #'identity
                 `(
                   ,(concat "* TODO " title)
                   ":PROPERTIES:"
                   ,(concat ":EXPORT_FILE_NAME: " (format-time-string "%Y-%m-%d-") fname)
                   ,(concat ":EXPORT_DATE: " (format-time-string "%Y-%m-%d"))
                   ":END:"
                   "%?\n")          ;Place the cursor here finally
                 "\n")))

  (add-to-list 'org-capture-templates '("h" "Hugo post"))
  (add-to-list 'org-capture-templates
               '("hq"                ;`org-capture' binding + hq
                 "Hugo post -- Quantum Computing"
                 entry
                 ;; It is assumed that below file is present in `org-directory'
                 ;; and that it has a "Quantum Computing" heading. It can even be a
                 ;; symlink pointing to the actual location of all-posts.org!
                 (file+olp "~/personnel-nextcloud/personal-site/content-org/all-posts.org" "Quantum Computing")
                 (function org-hugo-new-subtree-post-capture-template)))
  )
