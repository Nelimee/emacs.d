(defun nelimee/update-python-modeline ()
  (when (derived-mode-p 'python-mode)
    (doom-modeline-env-update-python)))

(use-package conda
  :config
  ;; Write the actual conda environment in the mode line.
  (setq-default mode-line-format (cons '(:exec conda-env-current-name) mode-line-format))
  ;; https://github.com/necaris/conda.el#basic-usage
  (setq conda-anaconda-home "/home/suau/miniconda3/")
  (setq conda-env-home-directory (expand-file-name "~/miniconda3/"))
  (setq conda-env-subdirectory "envs")
  :bind (("C-c e a" . conda-env-activate)
         ("C-c e d" . conda-env-deactivate)
         )
  :hook (;; Change python interpreter version on doom modeline when conda environment is activated
         (conda-postactivate . nelimee/update-python-modeline)
         ;; Change python interpreter version on doom modeline when conda environment is deactivated
         (conda-postdeactivate . nelimee/update-python-modeline)
         ;; Restart LSP server if started
         (conda-postactivate . lsp)
         (conda-postdeactivate . lsp-workspace-shutdown)
         )
  :straight (:host github :repo "necaris/conda.el"
                   :branch "main")
  )

;; https://aliquote.org/post/emacs-python3/
(setq python-shell-interpreter "python3")
(setq python-shell-interpreter-args "-m IPython --simple-prompt -i")

(add-hook 'python-mode-hook
      (lambda ()
        (setq indent-tabs-mode nil)
        (setq tab-width 4)
        (setq python-indent-offset 4)))

(use-package python-black
  :demand t
  :after python
  :hook (python-mode . python-black-on-save-mode))
