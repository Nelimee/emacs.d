;; https://gitlab.com/koral/gcmh

(use-package gcmh
  :config
  (gcmh-mode 1)
  (setq gcmh-verbose t)
  (setq gcmh-low-cons-threshold 8000000)    ;; 8MiB
  (setq gcmh-high-cons-threshold 50000000)  ;; 50MiB
  )
