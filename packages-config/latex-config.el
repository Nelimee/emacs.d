(use-package tex-site
  :defer t
  :straight auctex
  :config
  ;; Activate nice interface between RefTeX and AUCTeX
  (setq reftex-plug-into-AUCTeX t)
  ;; Load RefTeX
  (require 'reftex)
  ;; From https://tex.stackexchange.com/a/278060/166093
  (setq reftex-label-alist '(AMSTeX))
  ;; From https://tex.stackexchange.com/questions/139824/disabling-the-select-reference-format-menu-in-reftex
  (setq reftex-ref-macro-prompt nil)
  ;; From https://www.gnu.org/software/emacs/manual/html_node/reftex/Reference-Styles.html
  (setq reftex-ref-style-default-list '("Cleveref"))
  ;; Turn on RefTeX in AUCTeX
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook (lambda ()
                               ;; Turn on folding
                               ;; From https://www.gnu.org/software/auctex/manual/auctex/Folding.html
                               (TeX-fold-mode 1)
                               ;; Add a few extensions that should be considered as intermediate
                               ;; files that can be remove with "Clean".
                               (add-to-list 'LaTeX-clean-intermediate-suffixes "\\.maf")
                               (add-to-list 'LaTeX-clean-intermediate-suffixes "\\.mtc[0-9]*")))
  ;; Refresh the PDFView buffer after compilation
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)
  ;; to use pdfview with auctex
  ;; From https://emacs.stackexchange.com/a/21764
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
        TeX-source-correlate-start-server t) ;; not sure if last line is neccessary
  ;; Defaults options for AucTeX:
  (setq TeX-parse-self t)
  (setq TeX-auto-save t)
  (setq TeX-auto-untabify t)
  (with-eval-after-load "flycheck" (flycheck-add-next-checker 'tex-chktex 'textlint))
  )

;; From https://github.com/politza/pdf-tools/issues/528#issuecomment-564773133
(use-package pdf-tools
  :ensure t
  :mode  ("\\.pdf\\'" . pdf-view-mode)
  :config
  (setq-default pdf-view-display-size 'fit-page)
  (setq pdf-annot-activate-created-annotations t)
  (pdf-tools-install :no-query)
  (require 'pdf-occur))
