(add-hook 'js-mode-hook 'hs-minor-mode)

(global-set-key (kbd "C-c @ h a") 'hs-hide-all)
(global-set-key (kbd "C-c @ h b") 'hs-hide-block)
(global-set-key (kbd "C-c @ h l") 'hs-hide-level)
(global-set-key (kbd "C-c @ s a") 'hs-show-all)
(global-set-key (kbd "C-c @ s b") 'hs-show-block)
