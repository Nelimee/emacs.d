(use-package page-break-lines)


(use-package dashboard
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-banner-logo-title "Welcome Adrien!")
  (setq dashboard-startup-banner 'logo)
  ;; (setq dashboard-center-content t)
  ;; To customize which widgets are displayed.
  ;; This will add the recent files, bookmarks, projects, org-agenda
  ;; and registers widgets to your dashboard each displaying 5 items.
  (setq dashboard-items '((recents  . 10)
                          (bookmarks . 5)
                          ;; (projects . 5)
                          ;; (agenda . 5)
                          ;; (registers . 5)
                          ))
  ;; To add icons to the widget headings and their items
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  ;; To show navigator below the banner
  (setq dashboard-set-navigator t)
  ;; To show info about the packages loaded and the init time
  (setq dashboard-set-init-info t)

  ;; In addition to the above, configure initial-buffer-choice to show
  ;; Dashboard in frames created with emacsclient -c as follows:
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

  ;; To modify the widget heading name
  (setq dashboard-item-names '(("Recent Files:" . "Recently opened files:")
                               ("Agenda for today:" . "Today's agenda:")
                               ("Agenda for the coming week:" . "Agenda:")))
  )
